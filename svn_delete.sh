#!/bin/bash

#repo_branches_url="http://repo_name/branches/"
repo_branches_url=""
oldness="6 month ago"

olddate=`date --date="${oldness}" +%Y%m%d`

list=`svn list $repo_branches_url`

echo $list
echo "Branches to move:"

for branch in $list; do
	branchdate=`svn info --xml ${repo_branches_url}${branch} | grep -E -s '^<date>' | sed 's|^<date>\([0-9]\{4\}\)-\([0-9]\{2\}\)-\([0-9]\{2\}\).*</date>$|\1\2\3|g'`

	if [ ${branchdate} -lt ${olddate} ]; then
		echo $branch $branchdate
		svn move ${repo_branches_url}${branch} ${repo_branches_url}archive/${branch} --message "moving ${branch} to archive"
	fi
done
